import React from "react";
import styled from 'styled-components';

const Another = styled.button`
  position: fixed;
  bottom: 2rem;
  left: 50%;
  transform: translateX(-50%);
`

const HomePage = () => (
  <main className="main">
    <div className="quote">
      <blockquote>
        <p>Nulla vitae elit libero, a pharetra augue. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Maecenas sed diam eget risus varius blandit sit amet non magna. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
        <footer className="quote-footer">
          <cite>Author Name</cite>
        </footer>
      </blockquote>
    </div>
    <Another>
      Another Quote
    </Another>
  </main>
);

export default HomePage;
