import React from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

const Nav = styled.nav`
  align-items: center;
  display: flex;
  height: 77px;
  justify-content: space-around;

  div {
    display: flex;
    justify-content: center;
    width: 40%;

    &.logo {
      width: 20%;
    }
  }
  a {
    margin: 0 7px;
    padding: 7px 14px;
    text-decoration: none;
  }

  span {
    font-weight: 700;
    padding: 7px 14px;
  }
`

const Header = () => {
  const activeStyle = { color: "blue" };

  return (
    <Nav>
      <div>
        <NavLink to="/" activeStyle={activeStyle}>Home</NavLink>
        <NavLink to="/about" activeStyle={activeStyle}>About</NavLink>
      </div>
      <div className="logo">
        Kvothe Kingkiller
      </div>
      <div>
        <NavLink to="/characters" activeStyle={activeStyle}>Characters</NavLink>
        <NavLink to="/books" activeStyle={activeStyle}>Books</NavLink>
      </div>
    </Nav>
  )
}

export default Header;
