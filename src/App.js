import React from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import Header from './components/layout/Header';
import HomePage from './components/home/HomePage';
import AboutPage from './components/about/AboutPage';
import CharactersPage from './components/characters/CharactersPage';
import BooksPage from './components/books/BooksPage';
import CoursesPage from './components/courses/CoursesPage';
import PageNotFound from './404';

function App() {
  return (
    <div className="App">
      <Header />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/about" component={AboutPage} />
        <Route path="/characters" component={CharactersPage} />
        <Route path="/books" component={BooksPage} />
        <Route path="/courses" component={CoursesPage} />
        <Route component={PageNotFound} />
      </Switch>
    </div>
  );
}

export default App;
