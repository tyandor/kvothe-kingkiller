import produce from 'immer'

const currentState = [
  {
    first: "one",
    next: "two",
  },
  {
    second: "one",
    next: "two",
  }
]

const nextState = produce(currentState, draft => {
  console.log(draft[0])
})

console.log(nextState === currentState)