import { combineReducers } from 'redux';
import books from './bookReducer';
import courses from './courseReducer';

const rootReducer = combineReducers({
  books,
  courses
});

export default rootReducer;
